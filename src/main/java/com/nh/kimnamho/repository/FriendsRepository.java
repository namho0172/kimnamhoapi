package com.nh.kimnamho.repository;

import com.nh.kimnamho.entity.Friends;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FriendsRepository extends JpaRepository<Friends, Long> {
}
