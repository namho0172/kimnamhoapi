package com.nh.kimnamho.repository;

import com.nh.kimnamho.entity.Humanity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HumanityRepository extends JpaRepository<Humanity, Long> {
}
