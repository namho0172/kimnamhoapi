package com.nh.kimnamho.controller;

import com.nh.kimnamho.service.FriendsService;
import com.nh.kimnamho.service.HumanityService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequiredArgsConstructor
@RequestMapping("/all")
public class HumanityController {
    private final FriendsService friendsService;
    private final HumanityService humanityService;

    @PostMapping("/new/friends-id/{friendsId}")
    public String setHumanity(@PathVariable long )
}
