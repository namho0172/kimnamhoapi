package com.nh.kimnamho.controller;

import com.nh.kimnamho.model.friend.FriendsCreateRequest;
import com.nh.kimnamho.service.FriendsService;
import com.nh.kimnamho.service.HumanityService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/Friends")
public class FriendsController {
    private final FriendsService friendsService;
    private final HumanityService humanityService;

    @PostMapping("/join")
    public String setFriends(@RequestBody FriendsCreateRequest request) {
        friendsService.setFriends(request);
        return "OK";
    }

}
