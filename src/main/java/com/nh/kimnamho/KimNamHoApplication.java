package com.nh.kimnamho;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KimNamHoApplication {

	public static void main(String[] args) {
		SpringApplication.run(KimNamHoApplication.class, args);
	}

}
