package com.nh.kimnamho.service;

import com.nh.kimnamho.entity.Friends;
import com.nh.kimnamho.model.friend.FriendsCreateRequest;
import com.nh.kimnamho.repository.FriendsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
@RequiredArgsConstructor
public class FriendsService {
    private final FriendsRepository friendsRepository;

    private Friends getData(long id) { return friendsRepository.findById(id).orElseThrow(); }

    public void setFriends(FriendsCreateRequest request) {
        Friends addData = new Friends();
        addData.setName(request.getName());
        addData.setBrithDay(request.getBrithDay());
        addData.setPhoneNumber(request.getPhoneNumber());
        addData.setEtcMemo(request.getEtcMemo());
        addData.setIsCutOff(request.getIsCutOff());
        addData.setCutDate(LocalDate.now());
        addData.setReason(request.getReason());

        friendsRepository.save(addData);
    }
}
