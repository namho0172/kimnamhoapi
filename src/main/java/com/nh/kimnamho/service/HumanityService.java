package com.nh.kimnamho.service;

import com.nh.kimnamho.entity.Friends;
import com.nh.kimnamho.entity.Humanity;
import com.nh.kimnamho.model.humanity.HumanityCreateRequest;
import com.nh.kimnamho.repository.HumanityRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
@RequiredArgsConstructor
public class HumanityService {
    private final HumanityRepository humanityRepository;

    public void setHumanity(Friends friends,HumanityCreateRequest request) {
        Humanity addData = new Humanity();
        addData.setFriends(request.getFriends());
        addData.setType(request.getType());
        addData.setGiftType(request.getGiftType());
        addData.setAmount(request.getAmount());
        addData.setDay(LocalDate.now());

        humanityRepository.save(addData);
    }


}
