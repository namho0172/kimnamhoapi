package com.nh.kimnamho.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Friends {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String brithDay;

    @Column(nullable = false)
    private String phoneNumber;

    private String etcMemo;

    @Column(nullable = false)
    private String isCutOff;

    private LocalDate cutDate;

    private String reason;
}
