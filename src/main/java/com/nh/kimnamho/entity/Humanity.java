package com.nh.kimnamho.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Humanity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "friendsId", nullable = false)
    private Friends friends;

    @Column(nullable = false)
    private Boolean type;

    private String giftType;

    @Column(nullable = false)
    private Double amount;

    @Column(nullable = false)
    private LocalDate day;
}
