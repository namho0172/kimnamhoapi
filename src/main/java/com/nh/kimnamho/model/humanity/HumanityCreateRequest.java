package com.nh.kimnamho.model.humanity;

import com.nh.kimnamho.entity.Friends;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class HumanityCreateRequest {
    private Friends friends;
    private Boolean type;
    private String giftType;
    private Double amount;
    private LocalDate day;
}
