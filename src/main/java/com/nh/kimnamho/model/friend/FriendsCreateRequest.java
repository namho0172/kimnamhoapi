package com.nh.kimnamho.model.friend;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
@Getter
@Setter
public class FriendsCreateRequest {
    private String name;
    private String brithDay;
    private String phoneNumber;
    private String etcMemo;
    private String isCutOff;
    private LocalDate cutDate;
    private String reason;
}
